//
//  AppDelegate.h
//  FZViewDebuggingTest
//
//  Created by Fan Zhang on 6/9/14.
//  Copyright (c) 2014 Fan Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

