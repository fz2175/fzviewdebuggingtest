//
//  main.m
//  FZViewDebuggingTest
//
//  Created by Fan Zhang on 6/9/14.
//  Copyright (c) 2014 Fan Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
